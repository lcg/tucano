#ifndef __OCTREE__
#define __OCTREE__

#include <vector>
#include <Eigen/Dense>
#include "tucano/mesh.hpp"
#include "tucano/utils/math.hpp"

namespace Tucano
{


namespace Datastructures
{

class Octree;

/**
 * 
 **/
class OctreeNode {

	friend class Octree;

protected:	

	vector < int > child_id;

	vector < int > primitive_id;

	Eigen::Vector3f min_corner = Eigen::Vector3f::Zero();

	Eigen::Vector3f max_corner = Eigen::Vector3f::Zero();

	int level = 0;

public:

	OctreeNode () {}

	OctreeNode (const Eigen::Vector3f& min_c, const Eigen::Vector3f& max_c, int l = 0) {
		min_corner = min_c;
		max_corner = max_c;
		level = l;
	}	

	void split (Tucano::Mesh& mesh, vector < OctreeNode >& nodes) {

		Eigen::Vector3f center = (max_corner + min_corner)*0.5;

		// left bottom back
		nodes.push_back (OctreeNode (Eigen::Vector3f(min_corner[0], min_corner[1], min_corner[2]),
					Eigen::Vector3f(center[0], center[1], center[2]), level+1 ));
		

		// right bottom back
		nodes.push_back (OctreeNode (Eigen::Vector3f(center[0], min_corner[1], min_corner[2]),
					Eigen::Vector3f(max_corner[0], center[1], center[2]), level+1 ));
		

		// left top back
		nodes.push_back (OctreeNode (Eigen::Vector3f(min_corner[0], center[1], min_corner[2]),
					Eigen::Vector3f(center[0], max_corner[1], center[2]), level+1 ));
		

		// right top back
		nodes.push_back (OctreeNode (Eigen::Vector3f(center[0], center[1], min_corner[2]),
					Eigen::Vector3f(max_corner[0], max_corner[1], center[2]), level+1 ));
		

		// left bottom top
		nodes.push_back (OctreeNode (Eigen::Vector3f(min_corner[0], min_corner[1], center[2]),
					Eigen::Vector3f(center[0], center[1], max_corner[2]), level+1 ));
		

		// right bottom top
		nodes.push_back (OctreeNode (Eigen::Vector3f(center[0], min_corner[1], center[2]),
					Eigen::Vector3f(max_corner[0], center[1], max_corner[2]), level+1 ));
		

		// left top top
		nodes.push_back (OctreeNode (Eigen::Vector3f(min_corner[0], center[1], center[2]),
					Eigen::Vector3f(center[0], max_corner[1], max_corner[2]), level+1 ));
		

		// right top top
		nodes.push_back (OctreeNode (Eigen::Vector3f(center[0], center[1], center[2]),
					Eigen::Vector3f(max_corner[0], max_corner[1], max_corner[2]), level+1 ));
		
	}

	/**
	 * @brief returns one of the coordinates of the box
	 */
	Eigen::Vector3f getCorner (int id) {
		if (id == 0) // back - bottom - left
			return min_corner;
		if (id == 1) // back - bottom - right
			return Eigen::Vector3f(max_corner[0], min_corner[1], min_corner[2]);
		if (id == 2) // back - top - left
			return Eigen::Vector3f(min_corner[0], max_corner[1], min_corner[2]);
		if (id == 3) // back - top - right
			return Eigen::Vector3f(max_corner[0], max_corner[1], min_corner[2]);
		if (id == 4) // front - bottom - left
			return Eigen::Vector3f(min_corner[0], min_corner[1], max_corner[2]);
		if (id == 5) // front - bottom - right
			return Eigen::Vector3f(max_corner[0], min_corner[1], max_corner[2]);
		if (id == 6) // front - top - left
			return Eigen::Vector3f(min_corner[0], max_corner[1], max_corner[2]);
		//if (id == 7) // front - top - right
			return max_corner;
	}

	/**
	 * @brief Check if given point is inside node
	 */
	bool pointInside (const Eigen::Vector3f& point) {
		for (int i = 0; i < 3; ++i)
		{
			if (point[i] < min_corner[i] || point[i] > max_corner[i])
				return false;
		}
		return true;
	}

	/**
	 * @brief Test AABB triangle intersection
	 * not at all optimized!!!
	 **/
	bool intersectFace (Tucano::Mesh& mesh, int face_id) {	
		Tucano::Face& face = mesh.getFace(face_id);
		Eigen::Vector3f v0 = ((mesh.getShapeModelMatrix() * mesh.getVertex (  face.vertex_ids[0] ))).head(3);
		Eigen::Vector3f v1 = ((mesh.getShapeModelMatrix() * mesh.getVertex (  face.vertex_ids[1] ))).head(3);
		Eigen::Vector3f v2 = ((mesh.getShapeModelMatrix() * mesh.getVertex (  face.vertex_ids[2] ))).head(3);

		// if any vertex is inside the face intersects or is contained in the box
		if (pointInside(v0) || pointInside(v1) || pointInside(v2))
			return true;

		// just to get it working soon I am going to cheat and do a very brute force check
		// check if each box edge intersects the triangle
		Eigen::Vector3f intersection_point;
		if (Tucano::Math::segmentTriangleIntersection (getCorner(0), getCorner(1), v0, v1, v2, intersection_point)) return true;
		if (Tucano::Math::segmentTriangleIntersection (getCorner(0), getCorner(2), v0, v1, v2, intersection_point)) return true;
		if (Tucano::Math::segmentTriangleIntersection (getCorner(0), getCorner(4), v0, v1, v2, intersection_point)) return true;
		if (Tucano::Math::segmentTriangleIntersection (getCorner(1), getCorner(3), v0, v1, v2, intersection_point)) return true;
		if (Tucano::Math::segmentTriangleIntersection (getCorner(1), getCorner(5), v0, v1, v2, intersection_point)) return true;
		if (Tucano::Math::segmentTriangleIntersection (getCorner(2), getCorner(3), v0, v1, v2, intersection_point)) return true;
		if (Tucano::Math::segmentTriangleIntersection (getCorner(2), getCorner(6), v0, v1, v2, intersection_point)) return true;
		if (Tucano::Math::segmentTriangleIntersection (getCorner(3), getCorner(7), v0, v1, v2, intersection_point)) return true;
		if (Tucano::Math::segmentTriangleIntersection (getCorner(4), getCorner(5), v0, v1, v2, intersection_point)) return true;
		if (Tucano::Math::segmentTriangleIntersection (getCorner(4), getCorner(6), v0, v1, v2, intersection_point)) return true;
		if (Tucano::Math::segmentTriangleIntersection (getCorner(5), getCorner(7), v0, v1, v2, intersection_point)) return true;
		if (Tucano::Math::segmentTriangleIntersection (getCorner(6), getCorner(7), v0, v1, v2, intersection_point)) return true;

		return false;
	}

	bool intersectRay (const Eigen::Vector3f& dir, const Eigen::Vector3f& origin, Eigen::Vector3f& intersection_point, 
						vector < OctreeNode >& nodes, Tucano::Mesh& mesh, int& face_id ) 
	{
		// ray does not intersect this box
		if (!Tucano::Math::rayAABoxIntersection (dir, origin, min_corner, max_corner, intersection_point))
			return false;
		
		// test children
		Eigen::Vector3f test_point;
		float min_dist = 100000.0;

		// interior node, check children and get first intersection
		if (!child_id.empty())
		{
			int fid = -1;
			for (int i = 0; i < 8; ++i) // each child node
			{
				if (nodes[ child_id[i] ].intersectRay(dir, origin, test_point, nodes, mesh, fid)) // check intersection
				{
					if ((test_point - origin).norm() < min_dist) // get min intersection
					{
						min_dist = (test_point - origin).norm();
						intersection_point = test_point;
						face_id = fid;
					}
				}
			}
			if (min_dist < 100000.0) // intersected with some triangle
				return true;
		}
		else // test all faces in leaf node
		{			
			for (int i = 0; i < primitive_id.size(); ++i) // for each triangle
			{
				Tucano::Face face = mesh.getFace( primitive_id[i] );
    			if (Tucano::Math::rayTriangleIntersection(dir, origin, 
                                              (mesh.getShapeModelMatrix() * mesh.getVertex(face.vertex_ids[0])).head(3),
                                              (mesh.getShapeModelMatrix() * mesh.getVertex(face.vertex_ids[1])).head(3),
                                              (mesh.getShapeModelMatrix() * mesh.getVertex(face.vertex_ids[2])).head(3),
                                              test_point)) {
	      			if ((test_point - origin).norm() < min_dist)
	      			{
	        			min_dist = (test_point - origin).norm();
	        			intersection_point = test_point;
	        			face_id = primitive_id[i]; // closest face
	      			}
	      		}
    		}
    		if (min_dist < 100000.0)
    			return true;
		}
		// no intersection with children or primitives
		return false;
	}
};


/**
 * 
 **/
class Octree {
private:

	vector < OctreeNode > nodes;

	int max_elements = 3;

	int max_level = 5;

public:

	Octree () {
		// insert empty root node
		nodes.push_back ( OctreeNode() );	
	}

	Octree (const Eigen::Vector3f& min_corner, const Eigen::Vector3f& max_corner) {
		// insert empty root node
		nodes.push_back ( OctreeNode(min_corner, max_corner) );		
	}

	void setBounds (const Eigen::Vector3f& min_c, const Eigen::Vector3f& max_c) {
		nodes[0].min_corner = min_c;
		nodes[0].max_corner = max_c;
	}

	/**
	 * @brief inserts a triangle in the Octree
	 * Note that we receive the index of the current node to avoid
	 * loosing reference to the node when new ones are created (in case of a split)
	 * The vector might be resized and the reference lost, with the index there is no problem
	 **/
	void insertTriangle (Tucano::Mesh& mesh, int face_id, int nid) {
				
		// interior node
		if (!nodes[nid].child_id.empty())
		{		
			// insert into children
			for (int i = 0; i < nodes[nid].child_id.size(); ++i) {
				insertTriangle(mesh, face_id, nodes[nid].child_id[i] );
			}
		}
		// leaf node
		else
		{
			if (nodes[nid].intersectFace(mesh, face_id))
			{				
				nodes[nid].primitive_id.push_back (face_id);
			}

			// should we split this node?
			if (nodes[nid].primitive_id.size() > max_elements && nodes[nid].level < max_level)
			{
				
				// create new nodes
				nodes[nid].split(mesh, nodes);

				// insert indices of children
				for (int i = 0; i < 8; ++i)
				{				
					nodes[nid].child_id.push_back(nodes.size()-1-i);
				}

				// distribute primitives to children
				for (int i = 0; i < nodes[nid].primitive_id.size(); ++i)
				{
					for (int cid = 0; cid < nodes[nid].child_id.size(); ++cid)
					{			
						insertTriangle (mesh, nodes[nid].primitive_id[i], nodes[nid].child_id[cid]);
					}
				}
				nodes[nid].primitive_id.clear();
			}
		}
	}

	void insertMesh (Tucano::Mesh& mesh) {
		if (mesh.getNumberOfVertices() == 0)
			return;

		//get bounds
		Eigen::Vector3f min_coord = (mesh.getShapeModelMatrix() * mesh.getVertex(0)).head(3);
		Eigen::Vector3f max_coord = (mesh.getShapeModelMatrix() * mesh.getVertex(0)).head(3);
		for (int vid = 0; vid < mesh.getNumberOfVertices(); ++vid) {
			for (int i = 0; i < 3; ++i) {
				min_coord[i] = min( min_coord[i], (mesh.getShapeModelMatrix() * mesh.getVertex(vid))[i] );
				max_coord[i] = max( max_coord[i], (mesh.getShapeModelMatrix() * mesh.getVertex(vid))[i] );
			}
		}

		setBounds(min_coord, max_coord);

		for (int fid = 0; fid < mesh.getNumberOfFaces(); ++fid) {
			std::cout << "insert face " << fid << " /  " << mesh.getNumberOfFaces() << std::endl;
			insertTriangle (mesh, fid, 0);
		}

	}

	bool rayIntersection (const Eigen::Vector3f& dir, const Eigen::Vector3f& origin, Tucano::Mesh& mesh, Eigen::Vector3f& intersection_point, int& face_id) {
		return nodes[0].intersectRay(dir, origin, intersection_point, nodes, mesh, face_id);
	}

	bool importFromFile (string filename)
	{
    	filename.erase(std::remove(filename.begin(), filename.end(), '\n'), filename.end());
    	filename.erase(std::remove(filename.begin(), filename.end(), '\r'), filename.end());

    	ifstream in(filename.c_str(), ios::in);
    	if (!in)
    	{
        	std::cerr << "Cannot open " << filename.c_str() << std::endl; 
        	return false;
    	}

    	nodes.clear();
    	OctreeNode node;
    	std::string s;
        vector < std::string > tokens;
        for( std::string line; getline( in, line ); )
        {
            std::stringstream ss(line);

            if (ss.str().empty())
                continue;

            tokens.clear();
            while (std::getline(ss, s, ' '))
            {
                tokens.push_back (s);
            }

            if (tokens[0].compare("#") == 0)
            {
                continue;
            }
            else if (tokens[0].compare("MAX_ELEMENTS_PER_NODE") == 0)
            {
                max_elements = atof(tokens[1].c_str());
            }
            else if (tokens[0].compare("MAX_LEVEL") == 0)
            {
                max_level = atof(tokens[1].c_str());
            }
            else if (tokens[0].compare("NODE") == 0)
            {
                node = OctreeNode();
            }
            else if (tokens[0].compare("MIN_CORNER") == 0)
            {
            	Eigen::Vector3f minc (atof(tokens[1].c_str()), atof(tokens[2].c_str()), atof(tokens[3].c_str()));  
                node.min_corner = minc;                
            }
            else if (tokens[0].compare("MAX_CORNER") == 0)
            {
            	Eigen::Vector3f maxc (atof(tokens[1].c_str()), atof(tokens[2].c_str()), atof(tokens[3].c_str()));  
                node.max_corner = maxc;
            }
			else if (tokens[0].compare("NODE_LEVEL") == 0)
            {            	
                node.level = atoi(tokens[1].c_str());
            }
			else if (tokens[0].compare("CHILDREN") == 0)
            {
            	int num_children = atoi(tokens[1].c_str());
            	for (int i = 0; i < num_children; ++i)
            	{
            		node.child_id.push_back ( atoi(tokens[2+i].c_str()) );
            	}
            }
			else if (tokens[0].compare("PRIMITIVES") == 0)
            {
            	int num_primitives = atoi(tokens[1].c_str());
            	for (int i = 0; i < num_primitives; ++i)
            	{
            		node.primitive_id.push_back ( atoi(tokens[2+i].c_str()) );
            	}
            }
            else if (tokens[0].compare("ENDNODE") == 0)
            {
            	nodes.push_back(node);
            }
		}

		return true;

	}

	void exportToFile (string filename)
	{
    	ofstream out_stream;
    	out_stream.open(filename.c_str());
    	out_stream << "#OCTREE\n";
    	out_stream << "MAX_ELEMENTS_PER_NODE " << max_elements << "\n";
    	out_stream << "MAX_LEVEL " << max_level << "\n";


    	for (int i = 0; i < nodes.size(); ++i)
    	{
    		out_stream << "NODE " << i << "\n";
			out_stream << "MIN_CORNER " << nodes[i].min_corner[0] << " " << nodes[i].min_corner[1] << " " << nodes[i].min_corner[2] << "\n";
			out_stream << "MAX_CORNER " << nodes[i].max_corner[0] << " " << nodes[i].max_corner[1] << " " << nodes[i].max_corner[2] << "\n";
			out_stream << "NODE_LEVEL " << nodes[i].level << "\n";
    		out_stream << "CHILDREN " << nodes[i].child_id.size();
    		for (int c = 0; c < nodes[i].child_id.size(); ++c)
    		{
    			out_stream << " " << nodes[i].child_id[c];
    		}
    		out_stream << "\n";

    		out_stream << "PRIMITIVES " << nodes[i].primitive_id.size();
    		for (int c = 0; c < nodes[i].primitive_id.size(); ++c)
    		{
    			out_stream << " " << nodes[i].primitive_id[c];
    		}
    		out_stream << "\n";
    		out_stream << "ENDNODE " << i << "\n";
    	}
	}

};

}
}

#endif // OCTREE
